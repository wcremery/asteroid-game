﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour
{
    // The number of points won if the player pass through the gate
    private int m_points = 100;

    // Trigger if the gate collide something
    private void OnTriggerEnter(Collider p_other)
    {
        // Check if we collide with the player's space ship
        if(p_other.CompareTag("SpaceShip"))
        {
            p_other.GetComponent<ShipController>().UpdateScore(m_points);

            // Destroy a gate after the player pass through once
            if(!gameObject.Equals(null)) Destroy(gameObject); // short way for GetComponent<GameObject>()
        }
    }
}