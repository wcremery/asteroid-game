﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ShipController : MonoBehaviour {

    [Header("Settings")]
    private float m_speed = 100.0F;
    private float m_speed_max = 300.0F;
    private float m_speed_min = 10.0F;
    private float m_rotation_speed = 100.0F;
    private float m_ship_acceleration = 20.0F;    

    [Header("UI")]
    private int m_score = 0;
    private Text m_score_text;
    private int m_health = 100;
    private Text m_healthPercentage;
    private Image m_health_bar;
    private Image m_speed_bar;
    private Image m_speed_effect;
    private CameraController m_player_view;
    private Image m_aim_first_person;

    [Header("Lasers")]
    private AudioSource m_laser_sfx;
    private Transform m_spawn_laser_left_wing;
    private Transform m_spawn_laser_right_wing;
    private Transform m_spawn_laser_center_ship;
    private GameObject m_laser_prefab;
    private float m_laser_duration = 1.5F;
    private float m_laser_speed = 1000.0F;

    private void Awake()
    {
        InitializeUIComponents();
        InitializeLaserComponents();
        m_player_view = transform.Find("Main Camera").GetComponent<CameraController>();
    }

    private void InitializeLaserComponents()
    {
        m_laser_sfx = GetComponent<AudioSource>();
        m_spawn_laser_left_wing = transform.Find("SpawnLaserLeftWing").GetComponent<Transform>();
        m_spawn_laser_right_wing = transform.Find("SpawnLaserRightWing").GetComponent<Transform>();
        m_spawn_laser_center_ship = transform.Find("SpawnLaserCenterShip").GetComponent<Transform>();
        m_laser_prefab = Resources.Load("Prefabs/FlyerLaser") as GameObject;
    }

    private void InitializeUIComponents()
    {
        // transform.parent.Find do not works perphaps rectTransform cause trouble ?
        m_healthPercentage = GameObject.Find("UI/Health/RedBar/HealthText").GetComponent<Text>();
        m_health_bar = GameObject.Find("UI/Health/RedBar/GreenBar").GetComponent<Image>();
        m_score_text = GameObject.Find("UI/Score/ScoreText").GetComponent<Text>();
        m_speed_bar = GameObject.Find("UI/Speed/SpeedBar/SpeedIndicator").GetComponent<Image>();
        m_speed_effect = GameObject.Find("UI/MaximumSpeedEffect").GetComponent<Image>();
        m_aim_first_person = GameObject.Find("UI/Aim").GetComponent<Image>();
    }

    void Update()
    {
        if (!GameManager.m_game_is_paused) ManagePlayerInputs(); 
    }

    

    private void ManagePlayerInputs()
    {
        ManagePlayerRotation();
        ManagePlayerShoot();
        ManagePlayerAcceleration();
        ManagePlayerAim();
    }

    // Display viewfinder and follow player's mouse position
    private void ManagePlayerAim()
    {
        if (m_player_view.Player_in_cockpit)
        {
            // TODO : add boundaries
            m_aim_first_person.enabled = true;
            m_aim_first_person.GetComponent<Transform>().position = Input.mousePosition;
        }
        else m_aim_first_person.enabled = false;
    }

    private void ManagePlayerAcceleration()
    {
        float mouse_wheel = Input.mouseScrollDelta.y;
        float mouse_scale = 200.0F;

        if (mouse_wheel != 0) m_speed += mouse_wheel * mouse_scale * m_ship_acceleration * Time.deltaTime;
        if (m_speed > m_speed_max) m_speed = m_speed_max;
        else if (m_speed < m_speed_min) m_speed = m_speed_min;
        AccelerationBehaviour();
    }

    // Update UI with actual speed and change speed effect
    private void AccelerationBehaviour()
    {
        float speedPercentage = (m_speed - m_speed_min) / (m_speed_max - m_speed_min);

        m_speed_bar.fillAmount = speedPercentage;
        transform.Translate(Vector3.forward * m_speed * Time.deltaTime);
        m_speed_effect.color = new Color(1, 1, 1,speedPercentage);
    }

    private void ManagePlayerShoot()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (m_player_view.Player_in_cockpit)
            {
                FireFPS(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
            else
            {
                FireTPS(m_spawn_laser_left_wing.transform);
                FireTPS(m_spawn_laser_right_wing.transform);
            }
            PlayLaserSFX();
        }
    }

    private void PlayLaserSFX()
    {
        m_laser_sfx.pitch = Random.Range(1.0F, 1.5F);
        m_laser_sfx.Play();
    }

    // Manage laser destination in FPS view
    private void FireFPS(Vector3 p_laser_destination)
    {
        GameObject laser = Instantiate(m_laser_prefab, m_spawn_laser_center_ship.transform);
        laser.transform.Translate(p_laser_destination * m_laser_speed * Time.deltaTime);
        if (!laser.Equals(null)) Destroy(laser, m_laser_duration);
    }

    private void ManagePlayerRotation()
    {
        if (Input.GetKey(KeyCode.Q)) transform.Rotate(Vector3.forward * m_rotation_speed * Time.deltaTime);
        else if (Input.GetKey(KeyCode.D)) transform.Rotate(Vector3.back * m_rotation_speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S)) transform.Rotate(Vector3.left * m_rotation_speed * Time.deltaTime);
        else if (Input.GetKey(KeyCode.Z)) transform.Rotate(Vector3.right * m_rotation_speed * Time.deltaTime);
    }

    // Manage laser destination in FPS view
    private void FireTPS(Transform p_laser_spawn)
    {
        GameObject laser = Instantiate(m_laser_prefab, p_laser_spawn);
        laser.GetComponent<Rigidbody>().AddForce(laser.transform.forward * m_laser_speed, ForceMode.Impulse);
        if (!laser.Equals(null)) Destroy(laser, m_laser_duration);
    }

    // Reduces ship's health from given damages and apply changes to UI
    public void ApplyDamages(int p_damages)
    {
        // Reduce ship's amount of health
        m_health -= p_damages;
        if (m_health < 1) PlayerLose();

        // Apply changes to UI
        m_healthPercentage.text = m_health + "%";
        m_health_bar.fillAmount = m_health / 100.0F;
    }

    // Game Over behaviour
    private void PlayerLose()
    {
        m_health = 0;
        GameObject.Find("GameManager").GetComponent<GameManager>().GameOver();
    }

    // increase or decrease game's m_score from given points
    public void UpdateScore(int p_points)
    {
        m_score += p_points;
        m_score_text.text = m_score.ToString();
    }
}
