﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleScreenController : MonoBehaviour
{
    private Button m_start_button;
    private Button m_quit_button;

    private void Awake()
    {
        InitializeStartButton();
        InitializeQuitButton();
    }

    private void InitializeQuitButton()
    {
        m_quit_button = transform.Find("ImageBackground/ButtonArea/QuitButton").GetComponent<Button>();
        m_quit_button.onClick.AddListener(delegate { QuitGame(); });
    }

    private void QuitGame() => Application.Quit();

    private void InitializeStartButton()
    {
        m_start_button = transform.Find("ImageBackground/ButtonArea/StartButton").GetComponent<Button>();
        m_start_button.onClick.AddListener(delegate { StartCoroutine(LoadMainScene()); });
    }

    private IEnumerator LoadMainScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Main");
        
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
