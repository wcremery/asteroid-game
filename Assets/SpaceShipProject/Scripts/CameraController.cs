﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform m_cockpit_view;
    private Transform m_ship_view;
    private bool m_player_in_cockpit = false;

    public bool Player_in_cockpit { get => m_player_in_cockpit; }

    private void Awake()
    {
        m_cockpit_view = transform.parent.Find("CameraCockpitView").GetComponent<Transform>();
        m_ship_view = transform.parent.Find("CameraShipView").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update() => ManageCameraPosition();

    // Player is able to move from TPS to FPS view and vice versa
    private void ManageCameraPosition()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (!m_player_in_cockpit) transform.position = m_cockpit_view.position;
            else transform.position = m_ship_view.position;
            m_player_in_cockpit = !m_player_in_cockpit;
        }       
    }
}
