﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Asteroids")]
    private GameObject m_asteroid_type1;
    private GameObject m_asteroid_type2;

    [Header("Gate")]
    private GameObject m_gate;

    private int m_number_asteroid = 100;
    private int m_number_gates = 10;

    // Random limits
    private float m_range_position = 1000.0f;
    private Vector3 m_random_position;
    private float m_range_rotation = 180.0f;
    private Quaternion m_random_rotation;

    // UI
    private GameObject m_gameover_menu;
    private Button m_restart_button;
    private GameObject m_pause_menu;
    private Button m_resume_button;
    private Button m_quit_button;
    private Slider m_volume_slider;

    // system
    public static bool m_game_is_paused = false;

    private void Awake()
    {
        LoadAsteroids();
        SpawnAsteroids();
        LoadGates();
        SpawnGates();
        LoadUIComponents();
    }

    private void Update()
    {
        PlayerInputs();
    }

    private void PlayerInputs()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) PauseGame();
    }

    private void PauseGame()
    {
        StopGame();
        m_pause_menu.SetActive(true);
    }

    private static void StopGame()
    {
        m_game_is_paused = true;
        Time.timeScale = 0;
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
        m_game_is_paused = false;
        m_pause_menu.SetActive(false);
    }    

    private void LoadUIComponents()
    {
        LoadGameOverComponents();
        LoadPauseComponents();
    }

    private void LoadPauseComponents()
    {
        m_pause_menu = GameObject.Find("UI/PauseMenu");
        m_resume_button = GameObject.Find("UI/PauseMenu/PauseBackground/PauseButtons/ResumeButton").GetComponent<Button>();
        m_quit_button = GameObject.Find("UI/PauseMenu/PauseBackground/PauseButtons/QuitButton").GetComponent<Button>();
        m_volume_slider = GameObject.Find("UI/PauseMenu/PauseBackground/PauseButtons/VolumeArea/VolumeSlider").GetComponent<Slider>();
        m_resume_button.onClick.AddListener(delegate { ResumeGame(); });
    }

    private void LoadGameOverComponents()
    {
        m_gameover_menu = GameObject.Find("UI/GameOverMenu");
        m_restart_button = GameObject.Find("UI/GameOverMenu/ButtonArea/RestartButton").GetComponent<Button>();
        m_gameover_menu.SetActive(false);
    }

    private void LoadGates()
    {
        m_gate = Resources.Load<GameObject>("Prefabs/FlyerGate");
    }

    private void LoadAsteroids()
    {
        m_asteroid_type1 = Resources.Load<GameObject>("Prefabs/FlyerAsteroidA");
        m_asteroid_type2 = Resources.Load<GameObject>("Prefabs/FlyerAsteroidB");
    }

    private void SpawnGates()
    {
        for (int i = 0; i < m_number_gates; ++i) Spawn(m_gate);
    }

    private void SpawnAsteroids()
    {
        GameObject[] asteroids = { m_asteroid_type1, m_asteroid_type2 };
        int asteroidToSpawn;
        for (int i = 0; i < m_number_asteroid; ++i)
        {
            asteroidToSpawn = Random.Range(0, asteroids.Length);
            Spawn(asteroids[asteroidToSpawn]);
        }
    }

    // Spawn the given GameObject in a random position and random rotation
    private void Spawn(GameObject p_gameObject)
    {
        m_random_position = new Vector3(Random.Range(-m_range_position, m_range_position), 0, Random.Range(-m_range_position, m_range_position));
        m_random_rotation = new Quaternion(Random.Range(-m_range_rotation, m_range_rotation), Random.Range(-m_range_rotation, m_range_rotation), Random.Range(-m_range_rotation, m_range_rotation), 1);
        Instantiate(p_gameObject, m_random_position, m_random_rotation);
    }
    
    public void GameOver()
    {
        StopGame();
        m_gameover_menu.SetActive(true);
        m_restart_button.onClick.AddListener(delegate { ReloadScene(); });
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ResumeGame();
    }
}
