﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    // The number of health player gonna lose
    private int m_damage = 20;

    private GameObject m_asteroidNotYetExploded;
    private GameObject m_asteroidExplosionAnim;

    private void Awake()
    {
        m_asteroidNotYetExploded = transform.Find("ModelAsteroid").gameObject;
        m_asteroidExplosionAnim = transform.Find("FlyerAsteroidExplosion").gameObject;
    }

    private void OnTriggerEnter(Collider collision)
    {
        // Check if we collide with Space Ship
        if (collision.CompareTag("SpaceShip"))
        {
            // Decrement the player's amount of health when collision
            collision.GetComponent<ShipController>().ApplyDamages(m_damage);
            ManageAsteroidDestruction();
        }
        else if (collision.CompareTag("Laser")) ManageAsteroidDestruction();
    }

    private void ManageAsteroidDestruction()
    {
        if (!m_asteroidNotYetExploded.Equals(null)) Destroy(m_asteroidNotYetExploded);
        m_asteroidExplosionAnim.SetActive(true);
        m_asteroidExplosionAnim.GetComponent<ParticleSystem>().Play();

        // Destroy the asteroid animation after it ends
        if (!m_asteroidExplosionAnim.Equals(null)) Destroy(m_asteroidExplosionAnim, m_asteroidExplosionAnim.GetComponent<ParticleSystem>().main.duration);
        Destroy(this);
    }
}